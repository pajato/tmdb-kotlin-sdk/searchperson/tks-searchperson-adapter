package com.pajato.tks.search.person.adapter

import com.pajato.tks.common.adapter.Strategy
import com.pajato.tks.common.adapter.TmdbFetcher.fetch
import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.pager.core.PageData
import com.pajato.tks.pager.core.PageData.Companion.serializer
import com.pajato.tks.pager.core.PagedResultPerson
import com.pajato.tks.search.person.core.SearchPersonRepo

/**
 * TmdbSearchPersonRepo is a repository object for handling TMDb (The Movie Database) search operations specifically
 * for people. It caches search results to minimize API calls and improve performance.
 */
public object TmdbSearchPersonRepo : SearchPersonRepo {
    internal val cache: MutableMap<SearchKey, PageData<PagedResultPerson>> = mutableMapOf()

    /**
     * Retrieves a search page of persons based on the given search key.
     *
     * @param key The search key containing the query and page number to retrieve. It includes:
     *            - query: The search query string.
     *            - page: The specific page of results to retrieve.
     * @return A [PageData] object containing a list of [PagedResultPerson] and pagination information.
     */
    override suspend fun getSearchPagePerson(key: SearchKey): PageData<PagedResultPerson> {
        val path = "search/person"
        val extraParams: List<String> = listOf("query=${key.query}", "page=${key.page}")
        val personSerializer = PagedResultPerson.serializer()
        val serializer: Strategy<PageData<PagedResultPerson>> = serializer(personSerializer)
        val defaultPage = PageData<PagedResultPerson>(0, listOf(), 0, 0)
        return cache[key] ?: fetch(path, extraParams, key, serializer, defaultPage).also { cache[key] = it }
    }

    /**
     * Retrieves the search results for a given search key.
     *
     * @param key The search key containing the query and page number to retrieve.
     * @return A list of [PagedResultPerson] matching the search query.
     */
    override suspend fun getSearchResults(key: SearchKey): List<PagedResultPerson> = getSearchPagePerson(key).results
}

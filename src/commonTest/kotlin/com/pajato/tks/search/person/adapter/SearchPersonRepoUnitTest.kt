package com.pajato.tks.search.person.adapter

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.adapter.TmdbApiService.TMDB_BASE_API3_URL
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.InjectError
import com.pajato.tks.common.core.SearchKey
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.pager.core.PageData
import com.pajato.tks.pager.core.PagedResultPerson
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class SearchPersonRepoUnitTest : ReportingTestProfiler() {
    private val apiKey = "8G9B0WX31T2U7P0Q"
    private val urlConverterMap: MutableMap<String, String> = mutableMapOf()
    private val key: SearchKey = SearchKey("Person", "abc", 1)
    private val url = "$TMDB_BASE_API3_URL/search/person?api_key=$apiKey&query=abc&page=1"

    private lateinit var errorMessage: String
    private lateinit var errorExc: String
    private lateinit var errorExcMessage: String
    private var isFetched: Boolean = false

    @BeforeTest fun setUp() {
        TmdbFetcher.inject(::fetch, ::handleError)
        TmdbFetcher.inject(apiKey)
        errorMessage = ""
        errorExc = ""
        errorExcMessage = ""
        urlConverterMap.clear()
        isFetched = false
    }

    private fun fetch(url: String): String {
        val name = urlConverterMap[url] ?: fail("Key $url is not mapped to a test resource name!")
        isFetched = true
        return getJson(name).ifEmpty { throw IllegalStateException("No JSON available!") }
    }

    private fun getJson(name: String): String = javaClass.classLoader.getResource(name)?.readText() ?: ""

    private fun handleError(message: String, exc: Exception?) {
        errorMessage = message
        errorExc = if (exc == null) "null" else exc.javaClass.name
        errorExcMessage = if (exc == null) "null" else exc.message ?: "null"
    }

    @Test fun `When accessing a cached search page, verify the results`() {
        val resourceName = "person.json"
        val searchPage: PageData<PagedResultPerson> = jsonFormat.decodeFromString(getJson(resourceName))

        suspend fun assert() {
            val actualResults = TmdbSearchPersonRepo.getSearchResults(key)
            assertEquals(0, actualResults.size)
            assertFalse(isFetched)
        }

        TmdbSearchPersonRepo.cache[key] = searchPage
        runBlocking { assert() }
    }

    @Test fun `When accessing a non-cached search page, verify search page is fetched from TMDb`() {
        val resourceName = "person.json"
        val searchPage: PageData<PagedResultPerson> = jsonFormat.decodeFromString(getJson(resourceName))

        urlConverterMap[url] = resourceName
        runBlocking {
            assertEquals(searchPage, TmdbSearchPersonRepo.getSearchPagePerson(key))
            assertTrue(isFetched)
        }
    }

    @Test fun `When the repo is reset, verify inject error behavior`() {
        val key = SearchKey("Person", "xyzzy", 0)
        TmdbFetcher.reset()
        runBlocking { assertFailsWith<InjectError> { TmdbSearchPersonRepo.getSearchPagePerson(key) } }
    }
}
